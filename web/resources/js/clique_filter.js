function toggleLoadDB() {
    var enableLoadDB = document.getElementById("clique_config_form:load_db_checkbox").checked;
    var loadDBPanel = document.getElementById("load_db_row");
    if (enableLoadDB) {
        loadDBPanel.style.visibility = "visible";
    } else 
        loadDBPanel.style.visibility = "hidden";
}