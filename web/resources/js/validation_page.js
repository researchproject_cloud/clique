/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

function toggleCliqueDetail(cliqueElement)
{
    var cliqueId = cliqueElement.id.substring('clique_'.length, cliqueElement.id.length - 1);
    var detailDisplay = document.getElementById("cliqueDetail_" + cliqueId).style.display;
    
    if (detailDisplay === "none")
        detailDisplay = "inline";
    else detailDisplay = "none";
    
    document.getElementById("cliqueDetail_" + cliqueId).style.display = detailDisplay;
}

function changeCorrectState(clickedElement)
{
    var cliqueId = clickedElement.id.substr(clickedElement.id.length - 1, 1);
    var correctLinkName = "Click if incorrect";
    var checkIconVisible = "visible";

    if (document.getElementById("correctLink_" + cliqueId).innerHTML === "Click if incorrect")
    {
        correctLinkName = "Click if correct";
        checkIconVisible = "hidden";
    }

    document.getElementById("correctLink_" + cliqueId).innerHTML = correctLinkName;
    document.getElementById("checkIcon_" + cliqueId).style.visibility = checkIconVisible;
}
