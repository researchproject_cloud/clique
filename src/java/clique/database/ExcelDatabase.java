/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clique.database;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import Exception.FilloException;
import Fillo.*;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.logging.*;
import javax.naming.NamingException;
import model.other.DB;

/**
 *
 * @author Kazim
 */
public class ExcelDatabase {

    private static Fillo fillo = new Fillo();
    private static String xlsPath = "C:\\Test.xlsx";
    private static Logger logger = Logger.getLogger(ExcelDatabase.class.getName());
    
    public static void setXlsPath(String path) {
        xlsPath = path;
    }
    
    public static void storeData() {
        try {
            clearAllData();
            Logger.getLogger(ExcelDatabase.class.getName()).info("File path is: " + xlsPath);
            storeProviders();
            storeReviewers();
            storeReviews();
        } catch (Exception ex) {
            Logger.getLogger(ExcelDatabase.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    private static void clearAllData() throws SQLException, NamingException {
        DB db = new DB();
        db.queryUpdate("delete from review");
        db.queryUpdate("delete from reviewer");
        db.queryUpdate("delete from provider");
        Logger.getLogger(ExcelDatabase.class.getName()).info("Clear all data in database.");
    }

    private static void storeReviews() throws FilloException, ParseException, SQLException, NamingException {

        Connection connection = fillo.getConnection(xlsPath);
        Recordset recordset = connection.executeQuery("Select * from Review");
        DB db = new DB();
        DateFormat df = new SimpleDateFormat("MM/dd/yy");
        while (recordset.next()) {
            db.queryUpdate("insert into review (id,reviewer,content,review_datetime,rating_star,for_provider) values (?,?,?,?,?,?)",
                    Integer.parseInt(recordset.getField("id")),
                    recordset.getField("reviewer"),
                    recordset.getField("content"),
                    df.parse(recordset.getField("review_datetime")),
                    Integer.parseInt(recordset.getField("rating_star")),
                    recordset.getField("for_provider"));
        }

        recordset.close();
        connection.close();
    }
    
    private static void storeReviewers() throws FilloException, ParseException, SQLException, NamingException {

        Connection connection = fillo.getConnection(xlsPath);
        Recordset recordset = connection.executeQuery("Select * from Reviewer");
        DB db = new DB();
        while (recordset.next()) {
            db.queryUpdate("insert into reviewer (name,email,company_name) values (?,?,?)",
                    recordset.getField("name"),
                    recordset.getField("email"),
                    recordset.getField("company_name"));
        }

        recordset.close();
        connection.close();
    }
    
    private static void storeProviders() throws FilloException, ParseException, SQLException, NamingException {

        Connection connection = fillo.getConnection(xlsPath);
        Recordset recordset = connection.executeQuery("Select * from Provider");
        DB db = new DB();
        while (recordset.next()) {
            db.queryUpdate("insert into provider (id,company_name,description,company_email,weblink,reputation) values (?,?,?,?,?,?)",
                    Integer.parseInt(recordset.getField("id")),
                    recordset.getField("company_name"),
                    recordset.getField("description"),
                    recordset.getField("company_email"),
                    recordset.getField("weblink"),
                    Integer.parseInt(recordset.getField("reputation")));
        }

        recordset.close();
        connection.close();
    }
}
