/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clique.controllers;

import Exception.FilloException;
import Fillo.Connection;
import Fillo.Fillo;
import Fillo.Recordset;
import Utils.FilenameUtils;
import clique.database.ExcelDatabase;
import filters.CliqueFilter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.Serializable;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.model.DataModel;
import javax.faces.model.ListDataModel;
import javax.inject.Named;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.Part;
import model.clique.Clique;
import model.clique.CliqueDAO;
import model.provider.ProviderDAO;
import model.provider.ProviderDTO;
import org.apache.commons.io.IOUtils;
import org.apache.myfaces.custom.fileupload.UploadedFile;

/**
 * This class manages all the actions related to Clique algorithm as well as the
 * load_validation_page results.
 *
 * @author hien_quan
 */
@Named
@SessionScoped
public class CliqueFilterController implements Serializable {

    CliqueFilter cliqueFilter = null;
    List<Clique> cliques = null;
    List<Clique> inputCliques = null;
    List<ProviderDTO> providers;
    int correctPercentage = -1;
    int truePosNum = 0;
    int trueNegNum = 0;
    int falsePosNum = 0;
    int falseNegNum = 0;
    double h;
    double threshold;
    boolean filterDone = false;
    boolean reloadDB = false;
    private DataModel<Clique> cliqueModel;
    private UploadedFile dbFile;
    private UploadedFile cliqueUpFile;
    private String dbFileName;
    private String cliqueFileName;
    private int currentStage = 0;

    Logger logger = Logger.getLogger(CliqueFilterController.class.getName());

    public int getCurrentStage() {
        return currentStage;
    }

    public void setCurrentStage(int currentStage) {
        this.currentStage = currentStage;
    }

    public List<ProviderDTO> getProviders() {
        return providers;
    }

    public void setProviders(List<ProviderDTO> providers) {
        this.providers = providers;
    }

    public UploadedFile getDbFile() {
        return dbFile;
    }

    public void setDbFile(UploadedFile dbFile) {
        this.dbFile = dbFile;
    }

    public UploadedFile getCliqueUpFile() {
        return cliqueUpFile;
    }

    public void setCliqueUpFile(UploadedFile cliqueUpFile) {
        this.cliqueUpFile = cliqueUpFile;
    }

    private String uploadFile(UploadedFile upFile) {
        String folderPath = "C:\\Clique";
        String fileName = null;
        System.out.println("File type: " + upFile.getContentType());
        System.out.println("File name: " + upFile.getName());
        System.out.println("File size: " + upFile.getSize() + " bytes");

        // Prepare filename prefix and suffix for an unique filename in upload folder.
        String prefix = FilenameUtils.getBaseName(upFile.getName());
        String suffix = FilenameUtils.getExtension(upFile.getName());

        // Prepare file and outputstream.
        File file = null;
        OutputStream output = null;

        try {
            // Create file with unique name in upload folder and write to it.
            file = File.createTempFile(prefix + "_", "." + suffix, new File(folderPath));
            output = new FileOutputStream(file);
            IOUtils.copy(upFile.getInputStream(), output);
            fileName = folderPath + "\\" + file.getName();

            // Show succes message.
            FacesContext.getCurrentInstance().addMessage("clique_filter", new FacesMessage(
                    FacesMessage.SEVERITY_INFO, "File upload succeed!", null));
        } catch (IOException e) {
            // Cleanup.
            if (file != null) {
                file.delete();
            }

            // Show error message.
            FacesContext.getCurrentInstance().addMessage("clique_filter", new FacesMessage(
                    FacesMessage.SEVERITY_ERROR, "File upload failed with I/O error.", null));

            // Always log stacktraces (with a real logger).
            e.printStackTrace();
        } finally {
            IOUtils.closeQuietly(output);
        }
        return fileName;
    }

    /**
     * This method load database
     *
     * @return null
     */
    public String loadDB() {
        try {
            dbFileName = uploadFile(dbFile);
            ExcelDatabase.setXlsPath(dbFileName);
            ExcelDatabase.storeData();

            ProviderDAO providerDAO = new ProviderDAO();
            providers = new ArrayList<>();
            providers.addAll(providerDAO.loadAll());

        } catch (Exception ex) {
            logger.log(Level.SEVERE, ex.getMessage() + " - " + ex.getCause());
        }
        return null;
    }

    public DataModel getCliqueModel() {
        return cliqueModel;
    }

    public boolean isReloadDB() {
        return reloadDB;
    }

    public void setReloadDB(boolean reloadDB) {
        this.reloadDB = reloadDB;
    }

    public boolean isFilterDone() {
        return filterDone;
    }

    public double getH() {
        return h;
    }

    public void setH(double h) {
        this.h = h;
    }

    public double getThreshold() {
        return threshold;
    }

    public void setThreshold(double threshold) {
        this.threshold = threshold;
    }

    public int getCorrectPercentage() {
        return correctPercentage;
    }

    public CliqueFilter getCliqueFilter() {
        return cliqueFilter;
    }

    public List<Clique> getCliques() {
        return cliques;
    }

    /**
     * This function loads the content of the validation stage 1 page.
     */
    public void load_validation_stage_1() {
        FacesContext context = FacesContext.getCurrentInstance();
        HttpServletRequest request = (HttpServletRequest) context.getExternalContext().getRequest();

        if (request.getParameter("correct") == null || request.getParameter("id") == null) {
            return;
        }

        int clique_providerString = Integer.valueOf(request.getParameter("id"));
        boolean correct = Boolean.valueOf(request.getParameter("correct"));
        for (Clique clique : cliques) {
            if (clique.getProvider_string().equals(clique_providerString)) {
                clique.setCorrect(correct);
                break;
            }
        }
    }

    /**
     * This Method load filter method in the CliqueFilter Model
     *
     * @return null
     * @throws InterruptedException
     * @throws ParseException
     */
    public String filter() throws InterruptedException, ParseException, NamingException, SQLException {
        ProviderDAO providerDAO = new ProviderDAO();
        if (providers == null || providers.isEmpty()) {
            providers = new ArrayList<>();
            providers.addAll(providerDAO.loadAll());
        }

        cliqueFilter = new CliqueFilter(h, threshold);
        cliques = cliqueFilter.filter();
        cliqueModel = new ListDataModel<>(cliques);
        filterDone = true;
        if (cliqueFileName == null || cliqueFileName.isEmpty()) {
            currentStage = 1;
        } else currentStage = 2;
        cliqueFileName = null;
        return null;
    }

    public String loadCliqueInputData() throws FilloException {
        cliqueFileName = uploadFile(cliqueUpFile);
        Fillo fillo = new Fillo();
        Connection connection = fillo.getConnection(cliqueFileName);
        Recordset recordset = connection.executeQuery("Select * from Clique");
        inputCliques = new ArrayList<>();
        while (recordset.next()) {
            Clique clique = new Clique();
            clique.setProvider_string(recordset.getField("provider_string"));
            clique.setCorrect(true);
            clique.setStrong(Boolean.getBoolean(recordset.getField("strong")));
            inputCliques.add(clique);
        }
        return null;
    }

    /**
     * This function loads content of validation stage 2.
     *
     * @throws ParseException
     */
    public void load_validation_stage_2() throws ParseException {
        resetPosNegNums();

        for (Clique clique : cliques) {
            if (inputCliques.contains(clique)) {
                clique.setCorrect(true);
                updateReputation(clique);
                truePosNum++;
            } else {
                falsePosNum++;
            }
        }
        falseNegNum = inputCliques.size() - truePosNum;
    }

    /**
     * This method reset all the positive and negative values.
     */
    private void resetPosNegNums() {
        truePosNum = 0;
        trueNegNum = 0;
        falseNegNum = 0;
        falsePosNum = 0;
    }

    /**
     * This method load the content of clique_detail page with stage number and
     * provider name
     *
     * @param clique
     * @param stage_num
     * @return
     */
    public String showCliqueDetail(Clique clique, int stage_num) {
        return "clique_detail.xhtml?stage_num=" + stage_num + "&clique=" + clique.getProvider_string() + "faces-redirect=true";
    }

    /**
     * This method do the verification
     */
    public void doneVerification() throws SQLException, NamingException {
        int correctNum = 0;
        CliqueDAO cliqueDAO = new CliqueDAO();
        for (Clique clique : cliques) {
            logger.info(clique.toString());
            if (clique.isCorrect()) {
                cliqueDAO.insert(clique);
                updateReputation(clique);
                correctNum++;
            }
        }
        correctPercentage = correctNum * 100 / cliques.size();
        
    }

    private void updateReputation(Clique clique) {
        ProviderDAO providerDAO = new ProviderDAO();
        int minusMark = 30;
        if (clique.isStrong()) {
            minusMark = 70;
        }
        for (ProviderDTO provider : providers) {
            if ((provider.getReputation() == 100) && clique.getProvider_string().contains(provider.getCompany_name())) {
                try {
                    provider.setReputation(provider.getReputation() - minusMark);
                    providerDAO.updateReputation(provider);
                } catch (SQLException | NamingException ex) {
                    Logger.getLogger(CliqueFilterController.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }

    /**
     * This method gives the precision percentage
     *
     * @return
     */
    public int getPrecisionPercentage() {
        if (cliques.isEmpty()) {
            if (inputCliques.isEmpty()) {
                return 100;
            } else {
                return 0;
            }
        }

        return truePosNum * 100 / cliques.size();
    }

    /**
     * This method recalls percentage 100 or 0
     *
     * @return
     */
    public int getRecallPercentage() {
        if (inputCliques.isEmpty()) {
            if (cliques.isEmpty()) {
                return 100;
            } else {
                return 0;
            }
        }

        return truePosNum * 100 / inputCliques.size();
    }

    public int getTruePosNum() {
        return truePosNum;
    }

    public int getTrueNegNum() {
        return trueNegNum;
    }

    public int getFalsePosNum() {
        return falsePosNum;
    }

    public int getFalseNegNum() {
        return falseNegNum;
    }
}
