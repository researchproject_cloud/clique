/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clique.controllers;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.enterprise.context.RequestScoped;
import javax.inject.Named;
import javax.naming.NamingException;
import model.provider.ProviderDAO;
import model.provider.ProviderDTO;

/**
 * This controller class manages provider DAO.
 *
 * @author suzan, hien_quan
 */
@Named
@RequestScoped
public class ProviderListController {

    private final ProviderDAO providerDao = new ProviderDAO();
    private List<ProviderDTO> providers;

    public ProviderListController() {
        this.providers = new ArrayList<>();
    }

    public List<ProviderDTO> getProviders() {
        return providers;
    }

    /**
     * This method loads the provider
     *
     * @throws NamingException
     * @throws SQLException
     */
    public void loadProviders() throws NamingException, SQLException {
        providers.addAll(providerDao.loadAll());
    }

    public String getProvider(ProviderDTO provider) {
        return provider.getCompany_name();
    }

    public int getProviderId(ProviderDTO provider) {
        return provider.getId();
    }

    public String getProviderDesecription(ProviderDTO provider) {
        return provider.getDescription();
    }

    public int getReputation(ProviderDTO provider) {
        return provider.getReputation();
    }

    public String getprovideremail(ProviderDTO provider) {
        return provider.getCompany_email();
    }

    public String getproviderWeblink(ProviderDTO provider) {
        return provider.getWeblink();
    }
}
