/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clique.controllers;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.enterprise.context.*;
import javax.inject.Named;
import model.other.Criterion;

/**
 * This class manages search provider by criteria
 *
 * @author Kazim
 */
@Named
@SessionScoped
public class SearchProviderController implements Serializable {

    private List<Criterion> searchCriteria;

    public SearchProviderController() {
        searchCriteria = new ArrayList();
        searchCriteria.add(new Criterion());
    }

    public void setSearchCriteria(List<Criterion> searchCriteria) {
        this.searchCriteria = searchCriteria;
    }

    public List<Criterion> getSearchCriteria() {
        return searchCriteria;
    }

    /**
     * This function removes criteria from search criteria
     * @param criteria 
     */
    public void removeCriteria(Criterion criteria) {
        searchCriteria.remove(criteria);
    }
    /**
     * This function add search criteria to search criteria
     */
    public void addCriteria() {
        searchCriteria.add(new Criterion());
    }
}
