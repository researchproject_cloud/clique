/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clique.controllers;

import java.util.Date;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.enterprise.context.*;
import javax.inject.Named;
import model.clique.Clique;
import model.review.ReviewDTO;

/**
 * This backing controller manages load, correcting and find provider String
 *
 * @author Hien Quan
 */
@Named
@RequestScoped
public class CliqueController {

    Clique clique;
    int stageNum = 1;

    /**
     * This Method post construct the init method
     */
    @PostConstruct
    public void init() {
        clique = new Clique();
    }

    public int getStageNum() {
        return stageNum;
    }

    public void setStageNum(int stageNum) {
        this.stageNum = stageNum;
    }

    /**
     * This method loads the application with cliquefilterController
     *
     * @param stageNumber
     * @param providerString
     * @param cliqueFilterController
     */
    public void load(int stageNumber, String providerString, CliqueFilterController cliqueFilterController) {
        clique = findCliqueByProviderString(providerString, cliqueFilterController.getCliques());
        stageNum = stageNumber;
    }

    public Clique getClique() {
        return clique;
    }

    public String getReview(ReviewDTO review) {
        return review.getContent();
    }

    public Date getPublishDate(ReviewDTO review) {
        return review.getReview_datetime();
    }

    public String getReviewedProvider(ReviewDTO review) {
        return review.getFor_provider();
    }

    public int getRatingStar(ReviewDTO review) {
        return review.getRating_star();
    }

    /**
     * This Method corrects the open clique
     *
     * @param value
     * @param cliqueFilterController
     * @return
     */
    public String correct(boolean value, CliqueFilterController cliqueFilterController) {
        Clique opened_clique = findCliqueByProviderString(clique.getProvider_string(),
                cliqueFilterController.getCliques());
        opened_clique.setCorrect(value);
        return "clique_validation_stage1.xhtml?faces-redirect=true";
    }

    /**
     * This Method finds Clique by provider string
     *
     * @param providerString
     * @param cliques
     * @return
     */
    private Clique findCliqueByProviderString(String providerString, List<Clique> cliques) {
        for (Clique c : cliques) {
            if (c.getProvider_string().equals(providerString)) {
                return c;
            }
        }
        return null;
    }
}
