package filters;

import java.util.ArrayList;
import java.util.List;
import model.clique.Node;
import model.clique.NodeDirectRelation;
import model.other.Vertex;

/**
 * This class contains different methods to convert from node and its relevant information to vertex.
 *
 * @author Kazim, hien_quan
 */
public class NodeToVertex {

    /**
     * This Method convert Node to Vertex
     *
     * @param nodes
     * @return
     */
    public static List<Vertex> convertToVertex(List<Node> nodes) {
        List<Vertex> vertices = new ArrayList<>();
        for (Node node : nodes) {
            Vertex v = new Vertex();
            v.setNode(node);
            v.setSourceDistance(0);
            v.setPrevious(v);
            vertices.add(v);
        }
        return vertices;
    }

    /**
     * This method pass the list of Nodes to method converToVertex
     *
     * @param nodesdr
     * @param allVertex
     * @return
     */
    public static List<Vertex> convertToVertexDR(List<NodeDirectRelation> nodesdr, List<Vertex> allVertex) {

        List<Vertex> vertices = new ArrayList<>();
        for (NodeDirectRelation ndr : nodesdr) {
            for (Vertex v : allVertex) {
                if (v.getNode().equals(ndr.getToNode())) {
                    v.setEdgeDistance(ndr.getWeight());
                    vertices.add(v);
                    break;
                }
            }
        }
        return vertices;
    }
}
