/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package filters;

import java.util.*;
import model.other.Vertex;

/**
 * This Class implements Dijskstra Algorithm for finding minimum path between
 * nodes
 *
 * @author Kazim, hien_quan
 */
public class DijskstraAlgorithm {

    /**
     * This Method finds minimum distance between source node to every other
     * node
     *
     * @param sourceNode
     * @param vertices
     */
    public static void createDistanceBetweenNodes(Vertex sourceNode,
            List<Vertex> vertices) {

        NavigableSet<Vertex> vertexQueue = new TreeSet<>();
        sourceNode.setPrevious(sourceNode);
        sourceNode.sourceDistance = 0;
        vertexQueue.add(sourceNode);

        for (Vertex u : vertices) {
            if (u != sourceNode) {
                u.setPrevious(null);
                u.sourceDistance = Double.MAX_VALUE;
                vertexQueue.add(u);
            }
        }

        while (!vertexQueue.isEmpty()) {
            Vertex u = vertexQueue.pollFirst();
            if (u.sourceDistance == Double.MAX_VALUE) {
                continue;
            }
            List<Vertex> neighbors = NodeToVertex.convertToVertexDR(u.getNode()
                    .getDirectList(), vertices);

            for (Vertex v : neighbors) {
                double distanceThrough = u.sourceDistance + v.getEdgeDistance();

                if (distanceThrough < v.sourceDistance) {
                    vertexQueue.remove(v);
                    v.sourceDistance = distanceThrough;
                    v.setPrevious(u);
                    vertexQueue.add(v);
                }
            }
        }
    }
}
