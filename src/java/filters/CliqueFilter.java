package filters;

import java.sql.SQLException;
import java.util.*;
import javax.naming.NamingException;
import model.clique.*;
import model.other.*;
import model.provider.*;
import model.review.*;
import org.jboss.logging.Logger;

/**
 * This class implements clique algorithm and store the filter results.
 *
 * @author hien_quan, kazim
 */
public class CliqueFilter {

    /**
     * Total time to run the filter. Unit: millisecond
     */
    long executeTime;
    double h;
    double threshold;
    List<Node> nodes = new ArrayList();
    List<List<Node>> coreClusters = new ArrayList<>();
    List<Clique> strongCliques = new ArrayList<>();
    List<Clique> semiStrongCliques = new ArrayList<>();

    /* This list is filled with the current list of nodes for temporary usage of the algorithm only.
     * The nodes might be dynamically added into or removed out of this list.
     **/
    List<Node> coreNodes = new ArrayList<>();

    Logger logger = Logger.getLogger(CliqueFilter.class.getName());

    public CliqueFilter() {
    }

    public CliqueFilter(double h, double threshold) {
        this.h = h;
        this.threshold = threshold;
    }

    public long getExecuteTime() {
        return executeTime;
    }

    public double getH() {
        return h;
    }

    public double getThreshold() {
        return threshold;
    }

    /**
     * This Method filter clique according to algorithm Steps
     *
     * @return List of filtered cliques
     */
    public List<Clique> filter() {
        long startTime = System.currentTimeMillis();
        logger.info("Filter cliques");
        try {
            loadNodes();
            findMinPaths();
            calculateDensity();
            filterCoreNodes();

            List<Node> tempCoreNodes = new ArrayList<>();
            tempCoreNodes.addAll(coreNodes);
            findCoreClusters();
            coreNodes = new ArrayList<>();
            coreNodes.addAll(tempCoreNodes);

            classifyCoreClusters();

            strongCliques.addAll(semiStrongCliques);
            executeTime = System.currentTimeMillis() - startTime;
            return strongCliques;

        } catch (Exception ex) {
            logger.error(ex.getClass().getName() + " - " + ex.getMessage());
        }

        executeTime = System.currentTimeMillis() - startTime;
        return null;
    }

    /**
     * This Method load all the nodes using DAOs
     *
     * @throws NamingException
     * @throws SQLException
     */
    private void loadNodes() throws NamingException, SQLException {
        ProviderDAO providerDAO = new ProviderDAO();
        List<ProviderDTO> providers = providerDAO.loadAll();

        ReviewDAO reviewDAO = new ReviewDAO();
        for (ProviderDTO provider : providers) {
            Node node = new Node();
            node.setProviderName(provider.getCompany_name());
            node.setReputation(provider.getReputation());
            nodes.add(node);
        }

        for (Node node : nodes) {
            List<ReviewDTO> reviews = reviewDAO.findByReviewerCompanyName(node.getProviderName());
            NodeDirectRelation directRelation = new NodeDirectRelation();
            List<ReviewDTO> directReviews = new ArrayList();
            Node toNode = null;
            String currentForProvider = "";
            if (!reviews.isEmpty()) {
                currentForProvider = reviews.get(0).getFor_provider();
            }

            for (int i = 0; i < reviews.size(); i++) {
                if (currentForProvider.equals(reviews.get(i).getFor_provider())) {
                    directReviews.add(reviews.get(i));

                    if (i < (reviews.size() - 1)) {
                        continue;
                    }
                }

                //Assign value for the current direct relation
                double weight = calculateWeight(reviews);
                if (weight < h) {
                    directRelation.setReviews(directReviews);
                    directRelation.setWeight(weight);
                    toNode = findNodeByProviderName(currentForProvider);
                    directRelation.setToNode(toNode);
                    node.getDirectList().add(directRelation);
                    toNode.getRevDirectList().add(node);
                }

                // Create and initilize data for a new direct relation for the new for_provider
                currentForProvider = reviews.get(i).getFor_provider();
                directRelation = new NodeDirectRelation();
                directReviews = new ArrayList<>();
                directReviews.add(reviews.get(i));
            }
        }

    }

    /**
     * Find node by provider node
     *
     * @param name
     * @return Node
     */
    private Node findNodeByProviderName(String name) {
        for (Node node : nodes) {
            if (node.getProviderName().equals(name)) {
                return node;
            }
        }
        return null;
    }

    /**
     * Calculate weight from numbers of reviews given to provider
     *
     * @param reviews
     * @return weight
     */
    private double calculateWeight(List<ReviewDTO> reviews) {
        double totalRatingStar = 0;
        for (ReviewDTO review : reviews) {
            totalRatingStar += review.getRating_star();
        }
        return 1.0 / (totalRatingStar / reviews.size());
    }

    /**
     * Find minimum path from source to every other node using dijsktra
     * algorithm
     */
    private void findMinPaths() {

        List<Vertex> vertices = NodeToVertex.convertToVertex(nodes);//= List of All the Nodes;

        for (Vertex u : vertices) {
            DijskstraAlgorithm.createDistanceBetweenNodes(u, vertices);

            for (Vertex v : vertices) {
                if (v.getSourceDistance() <= 0 || v.getSourceDistance() == Double.MAX_VALUE) {
                    continue;
                }
                addToRegionLists(u, v);
            }
        }
    }

    /**
     * Create path and add path to region list
     *
     * @param source
     * @param dest
     */
    private void addToRegionLists(Vertex source, Vertex dest) {
        double influence = findInfluence(dest.getSourceDistance());
        if (influence > 0) {
            Path newPath = new Path(source.getNode(), dest.getNode(), influence);
            source.getNode().getRegionList().add(newPath);
            dest.getNode().getRevRegionList().add(newPath);
        }
    }

    /**
     * Find influence from the shortest distance found between source and
     * destination
     *
     * @param shortestDis
     * @return
     */
    private double findInfluence(double shortestDis) {
        double x = shortestDis / h;
        if (x <= 1) {
            return (1 - (x * x)) * 3 / 4;
        }
        return 0;
    }

    /**
     * Calculate Density from reverse region list
     *
     */
    private void calculateDensity() {
        for (Node node : nodes) {
            double density = 0;

            for (Path path : node.getRevRegionList()) {
                density += path.getInfluence();
            }
            node.setDensity(density);
        }
    }

    /**
     * Filter core nodes by checking threshold
     */
    private void filterCoreNodes() {
        coreNodes = new ArrayList<>();
        coreNodes.addAll(nodes);

        for (Node node : nodes) {
            if (node.getDensity() < threshold) {
                removeNodeFromDirectRelations(node);
                coreNodes.remove(node);
            }
        }
    }

    /**
     * Remove all direct and reverse direct relations related to the given node.
     *
     * @param node node that needs to be removed
     */
    private void removeNodeFromDirectRelations(Node node) {
        for (NodeDirectRelation directRelation : node.getDirectList()) {
            directRelation.getToNode().getRevDirectList().remove(node);
        }

        for (Node revDirectNode : node.getRevDirectList()) {
            NodeDirectRelation removeDirectRelation = null;
            for (NodeDirectRelation directRelation : revDirectNode.getDirectList()) {
                if (directRelation.getToNode().equals(node)) {
                    removeDirectRelation = directRelation;
                    break;
                }
            }

            revDirectNode.getDirectList().remove(removeDirectRelation);
        }
    }

    /**
     * This method find core clustures
     */
    private void findCoreClusters() {
        List<Node> coreCluster = new ArrayList<>();

        Node startNode = coreNodes.get(0);
        coreCluster.add(startNode);
        coreNodes.remove(startNode);

        expandClusterFromNode(startNode, coreCluster);
        coreClusters.add(coreCluster);

        if (!coreNodes.isEmpty()) {
            findCoreClusters();
        }
    }

    /**
     * This Method expands Cluster from nodes.
     *
     * @param node
     * @param coreCluster
     */
    private void expandClusterFromNode(Node node, List<Node> coreCluster) {
        for (NodeDirectRelation directRelation : node.getDirectList()) {
            Node toNode = directRelation.getToNode();
            if (coreCluster.contains(toNode)) {
                continue;
            }

            coreCluster.add(toNode);
            coreNodes.remove(toNode);
            expandClusterFromNode(toNode, coreCluster);
        }

        for (Node revDirectNode : node.getRevDirectList()) {
            if (coreCluster.contains(revDirectNode)) {
                continue;
            }

            coreCluster.add(revDirectNode);
            coreNodes.remove(revDirectNode);
            expandClusterFromNode(revDirectNode, coreCluster);
        }
    }

    /**
     * This Method classifies all the Core Clusters
     */
    private void classifyCoreClusters() {
        for (List<Node> cluster : coreClusters) {
            NavigableSet<Node> sortedNodes = findReachableNodeList(cluster);

            List<Node> onlyDirectNodes = new ArrayList<>();
            List<Node> onlyRevDirectNodes = new ArrayList<>();
            findAndRemoveOneDirectNodes(sortedNodes, onlyDirectNodes, onlyRevDirectNodes);

            logger.info("Start finding strong cliques");
            // Find strong clusters
            NavigableSet<Node> tempSortedNodes = new TreeSet<>(new ReachableNodeListComparator());
            tempSortedNodes.addAll(sortedNodes);
            while (!tempSortedNodes.isEmpty()) {
                Node node = tempSortedNodes.pollLast();
                List<Node> strongCore = prepareStrongCore(node, onlyDirectNodes, onlyRevDirectNodes);
                if (strongCore.size() < 2) {
                    continue;
                }

                for (Node strongCoreNode : strongCore) {
                    tempSortedNodes.remove(strongCoreNode);
                }

                Clique clique = new Clique(strongCore);
                clique.setStrong(true);
                strongCliques.add(clique);
            }
            logger.info("Finish finding strong cliques: " + strongCliques.size());

            // Find semi strong clusters
            logger.info("Start finding semi strong");
            for (Node node : onlyDirectNodes) {
                semiStrongCliques.add(prepareSemiStrongClique(node));
                onlyRevDirectNodes.removeAll(onlyRevDirectNodes);
            }
            for (Node node : onlyRevDirectNodes) {
                for (Node startNode : sortedNodes) {
                    if (startNode.getReachableNodes().contains(node)) {
                        semiStrongCliques.add(prepareSemiStrongClique(node));
                    }
                }
            }
            logger.info("Finish finding semi strong: " + semiStrongCliques.size());
        }
        logger.info("Example of strong clique: " + strongCliques.get(0).getProvider_string());
    }

    /**
     * This Method prepares List of Strong Cores found
     *
     * @param startNode
     * @param onlyDirectNodes
     * @param onlyRevDirectNodes
     * @return resultList
     */
    private List<Node> prepareStrongCore(Node startNode, List<Node> onlyDirectNodes, List<Node> onlyRevDirectNodes) {
        List<Node> resultList = new ArrayList<>();
        resultList.addAll(startNode.getReachableNodes());
        if (!resultList.contains(startNode)) {
            resultList.add(startNode);
        }
        resultList.removeAll(onlyDirectNodes);
        resultList.removeAll(onlyRevDirectNodes);

        return resultList;
    }

    private Clique prepareSemiStrongClique(Node node) {
        List<Node> semiStrongCore = new ArrayList<>();
        semiStrongCore.addAll(node.getReachableNodes());
        if (!semiStrongCore.contains(node)) {
            semiStrongCore.add(node);
        }
        Clique clique = new Clique(semiStrongCore);
        clique.setStrong(false);
        return clique;
    }

    /**
     * Find list of reachable nodes for each node in the current core node list.
     */
    private NavigableSet<Node> findReachableNodeList(List<Node> coreCluster) {
        logger.info("Start finding reachable node list.");
        NavigableSet<Node> doneNodes = new TreeSet<>(new ReachableNodeListComparator());

        for (Node node : coreCluster) {
            recursiveFindDirectNodes(node, node, doneNodes);
            doneNodes.add(node);
        }
        logger.info("Finish finding reachable node list: ");
        for (Node node : doneNodes.last().getReachableNodes()) {
            logger.info(node.getProviderName());
        }
        return doneNodes;
    }

    /**
     * This method finds recursively direct nodes
     *
     * @param sourceNode
     * @param node
     * @param doneNodes
     */
    private void recursiveFindDirectNodes(Node sourceNode, Node node, NavigableSet<Node> doneNodes) {
        logger.info("Start recursive find direct node: " + node.getProviderName());
        ListUtil<Node> listUtil = new ListUtil<>();

        for (NodeDirectRelation direct : node.getDirectList()) {
            if (sourceNode.getReachableNodes().contains(direct.getToNode())) {
                continue;
            }
            sourceNode.getReachableNodes().add(direct.getToNode());
            if (doneNodes.contains(direct.getToNode())) {
                listUtil.addWithoutDuplicate(sourceNode.getReachableNodes(), direct.getToNode().getReachableNodes());
                continue;
            }

            recursiveFindDirectNodes(sourceNode, direct.getToNode(), doneNodes);
            logger.info("Done recursive find direct node");
        }
    }

    /**
     * This method finds and remove one direct nodes from coreCluster
     *
     * @param coreCluster
     * @param onlyDirectNodes
     * @param onlyRevDirectNodes
     * @return onyDirectNodes
     */
    private List<Node> findAndRemoveOneDirectNodes(NavigableSet<Node> coreCluster,
            List<Node> onlyDirectNodes, List<Node> onlyRevDirectNodes) {

        logger.info("Start to find and remove one direct nodes");
        List<Node> tempCluster = new ArrayList<>();
        tempCluster.addAll(coreCluster);
        for (Node node : tempCluster) {
            boolean isDirectEmpty = node.getDirectList().isEmpty();
            boolean isRevDirectEmpty = node.getRevDirectList().isEmpty();

            if (isDirectEmpty || isRevDirectEmpty) {
                coreCluster.remove(node);
                if (isRevDirectEmpty && !isDirectEmpty) {
                    onlyDirectNodes.add(node);
                } else if (!isRevDirectEmpty && isDirectEmpty) {
                    onlyRevDirectNodes.add(node);
                }
            }
        }
        logger.info("Finish to find and remove one direct nodes, num of direct nodes: " + onlyDirectNodes.size() + ", num of rev direct: " + onlyRevDirectNodes.size());
        return onlyDirectNodes;
    }
}
