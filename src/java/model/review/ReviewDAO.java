/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.review;

import model.other.DB;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import javax.naming.NamingException;

/**
 *
 * @author Suzan Aziz
 */
public class ReviewDAO {

    public ArrayList<ReviewDTO> loadAll() throws NamingException, SQLException {
        ArrayList<ReviewDTO> reviews = new ArrayList<>();

        try (DB db = new DB();) {

            ResultSet rs = db.query("select * from review");
            while (rs.next()) {
                reviews.add(createDTO(rs));
            }
        }
        return reviews;
    }
    
    public ArrayList<ReviewDTO> findByReviewerCompanyName(String reviewerCompanyName) throws NamingException, SQLException {
        ArrayList<ReviewDTO> reviews = new ArrayList<>();

        try (DB db = new DB();) {

            ResultSet rs = db.query("select rv.* from review rv, reviewer rver where "
                    + "rv.reviewer = rver.email and "
                    + "rver.company_name = ? "
                    + "order by rv.FOR_PROVIDER", reviewerCompanyName);
            while (rs.next()) {
                reviews.add(createDTO(rs));
            }
        }
        return reviews;
    }

    private ReviewDTO createDTO(ResultSet rs) throws SQLException {
        ReviewDTO review = new ReviewDTO();

        review.setId(rs.getInt("id"));
        review.setReviewer(rs.getString("reviewer"));
        review.setContent(rs.getString("content"));
        review.setReview_datetime(rs.getDate("review_datetime"));
        review.setRating_star(rs.getInt("rating_star"));
        review.setFor_provider(rs.getString("for_provider"));
        return review;
    }
}
