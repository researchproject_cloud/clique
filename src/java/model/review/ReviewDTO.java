package model.review;

import java.util.Date;
import javax.validation.constraints.*;

/**
 * This class is for creating get and set function for Reviews DB records. 
 * @author Suzan 
 */
public class ReviewDTO {
    private int id;
    private String reviewer;
    private String content;
    private Date review_datetime;
    private int rating_star;
    private String for_provider;

    public ReviewDTO() {
    }

    public ReviewDTO(int id, String reviewer, String content, Date review_datetime, int rating_star, String for_provider) {
        this.id = id;
        this.reviewer = reviewer;
        this.content = content;
        this.review_datetime = review_datetime;
        this.rating_star = rating_star;
        this.for_provider = for_provider;
    }

    @NotNull
    public int getId() {
        return id;
    }
    
    @NotNull
    @Size(max = 255)
    public String getReviewer() {
        return reviewer;
    }

    @NotNull
    @Size(max = 5000)
    public String getContent() {
        return content;
    }

    @NotNull
    @Past
    public Date getReview_datetime() {
        return review_datetime;
    }

    @NotNull
    @Size(max = 255)
    public int getRating_star() {
        return rating_star;
    }
    
    @NotNull
    public String getFor_provider() {
        return for_provider;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setReviewer(String reviewer) {
        this.reviewer = reviewer;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public void setReview_datetime(Date review_datetime) {
        this.review_datetime = review_datetime;
    }

    

    public void setRating_star(int rating_star) {
        this.rating_star = rating_star;
    }

    public void setFor_provider(String for_provider) {
        this.for_provider = for_provider;
    }

    @Override
    public String toString() {
        return "ReviewDTO{" + "id=" + id + ", reviewer=" + reviewer + ", content=" + content + ", review_datetime=" + review_datetime + ", rating_star=" + rating_star + ", for_provider=" + for_provider + '}';
    }
    
    
}
