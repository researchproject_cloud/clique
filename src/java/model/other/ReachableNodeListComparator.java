/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.other;

import java.util.Comparator;
import model.clique.Node;

/**
 *
 * @author hien_quan
 */
public class ReachableNodeListComparator implements Comparator<Node> {

    @Override
    public int compare(Node o1, Node o2) {
        return Integer.compare(o1.getReachableNodes().size(), o2.getReachableNodes().size());
    }

}
