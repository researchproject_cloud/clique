/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.other;

import java.util.Objects;
import model.clique.Node;

/**
 * This Vertex is used in Dijskstra algorithm. Each vertex is corresponding to a node in graph.
 * @author Kazim
 */
public class Vertex implements Comparable<Vertex> {

    private Node node;
    private Vertex previous;
    public double sourceDistance = Double.MAX_VALUE;
    private double edgeDistance;

    public Vertex(Node node) {
        this.node = node;
    }

    public Vertex() {
    }

    public Node getNode() {
        return node;
    }

    public void setNode(Node node) {
        this.node = node;
    }

    public Vertex getPrevious() {
        return previous;
    }

    public void setPrevious(Vertex previous) {
        this.previous = previous;
    }

    public double getSourceDistance() {
        return sourceDistance;
    }

    public void setSourceDistance(double sourceDistance) {
        this.sourceDistance = sourceDistance;
    }

    public double getEdgeDistance() {
        return edgeDistance;
    }

    public void setEdgeDistance(double edgeDistance) {
        this.edgeDistance = edgeDistance;
    }

    @Override
    public int compareTo(Vertex other) {
        int distanceCompare = Double.compare(sourceDistance, other.sourceDistance);
        if (distanceCompare == 0) {
            if (node.equals(other.getNode())) {
                return 0;
            } else {
                return 1;
            }
        }
        return distanceCompare;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Vertex other = (Vertex) obj;
        return Objects.equals(this.node, other.node);
    }

    
}
