/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.other;

import java.io.Serializable;

/**
 *
 * @author HP
 */
public class Group implements Serializable{
    
    private String name;
    private int accuracy;
    
    public String getName() {
        return name;
    }

    public int getAccuracy() {
        return accuracy;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setAccuracy(int accuracy) {
        this.accuracy = accuracy;
    }
    
}
