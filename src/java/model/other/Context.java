/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.other;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

/**
 * This class provides some common methods used in the project.
 * @author hien_quan
 */
public class Context {
    public static HttpServletRequest getRequest() {
        FacesContext context = FacesContext.getCurrentInstance();
        return (HttpServletRequest)context.getExternalContext().getRequest();
    }
    
    public static void showError(String clientID, String message) {
        FacesContext context = FacesContext.getCurrentInstance();
        context.addMessage(clientID, new FacesMessage(message));
    }
}
