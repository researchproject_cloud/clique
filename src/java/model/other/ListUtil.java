/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.other;

import java.util.List;

/**
 * This class contains all util functions related to List
 * @author hien_quan
 * @param <T>
 */
public class ListUtil<T extends Object> {
    
    public void addWithoutDuplicate(List<T> sourceList, List<T> addList) {
        for (T obj : addList) {
            if (sourceList.contains(obj)) {
                continue;
            }
            sourceList.add(obj);
        }
    }
}
