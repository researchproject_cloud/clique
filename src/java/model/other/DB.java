package model.other;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

/**
 * This class contains supported methods for executing queries in database.
 *
 * @author Ryan Heise
 * @author Suzan Aziz
 */
public class DB implements AutoCloseable {

    private Connection cn;

    /**
     * Create new DB.
     *
     * @throws SQLException
     * @throws NamingException
     */
    public DB() throws SQLException, NamingException {
        DataSource ds = InitialContext.doLookup("jdbc/clique");
        cn = ds.getConnection();
    }

    /**
     * Execute query which has returned value only.
     *
     * @param queryStr query string.
     * @param params values to add into the query string.
     * @return the ResultSet of executing the query.
     * @throws SQLException
     */
    public ResultSet query(String queryStr, Object... params) throws SQLException {
        PreparedStatement ps = cn.prepareStatement(queryStr);
        return setParams(ps, params).executeQuery();
    }

    /**
     * Execute query which has not return value only.
     *
     * @param queryStr the query string.
     * @param params values to add into the query string.
     * @throws SQLException
     */
    public void queryUpdate(String queryStr, Object... params) throws SQLException {
        PreparedStatement ps = cn.prepareStatement(queryStr);
        setParams(ps, params).executeUpdate();
    }
    
    /**
     * Add values of params into the query string.
     *
     * @param ps the prepared statement.
     * @param params collection of parameters to be added into the query.
     * @return the prepared statement after adding parameters.
     * @throws SQLException
     */
    private PreparedStatement setParams(PreparedStatement ps, Object... params) throws SQLException {
        int index = 1;
        for (Object param : params) {
            if (param.getClass() == String.class) {
                ps.setString(index, param.toString());
            } else if (param.getClass() == Integer.class) {
                ps.setInt(index, (int) param);
            } else if (param.getClass() == Date.class) {
                ps.setDate(index, new java.sql.Date(((Date) param).getTime()));
            } else if (param.getClass() == Boolean.class) {
                ps.setBoolean(index, (boolean) param);
            }
            index++;
        }
        return ps;
    }

    @Override
    public void close() throws SQLException {
        cn.close();
    }
}
