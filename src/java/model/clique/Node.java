package model.clique;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import model.review.ReviewDTO;

/**
 * This class represents Model object for Node providing
 * providername,reputation, regionLists,revRegionList,DirectList and
 * revDirectList.
 *
 * @author Hien
 */
public class Node {

    String providerName;
    int reputation = -1;
    boolean reputationChanged = false;
    List<Node> reachableNodes = new ArrayList<>();
    List<Path> regionList = new ArrayList<>();
    List<Path> revRegionList = new ArrayList<>();
    List<NodeDirectRelation> directList = new ArrayList<>();
    List<Node> revDirectList = new ArrayList<>();
    double density = 0;
    boolean isOneDirectNode = false;

    public boolean isIsOneDirectNode() {
        return isOneDirectNode;
    }

    public void setIsOneDirectNode(boolean isOneDirectNode) {
        this.isOneDirectNode = isOneDirectNode;
    }

    public String getProviderName() {
        return providerName;
    }

    public void setProviderName(String providerName) {
        this.providerName = providerName;
    }

    public int getReputation() {
        return reputation;
    }

    public void setReputation(int reputation) {
        if ((this.reputation > -1) && (this.reputation != reputation)) {
            reputationChanged = true;
        }

        this.reputation = reputation;
    }

    public boolean isReputationChanged() {
        return reputationChanged;
    }

    public List<Node> getReachableNodes() {
        return reachableNodes;
    }

    public List<Path> getRegionList() {
        return regionList;
    }

    public void setRegionList(List<Path> regionList) {
        this.regionList = regionList;
    }

    public List<Path> getRevRegionList() {
        return revRegionList;
    }

    public void setRevRegionList(List<Path> revRegionList) {
        this.revRegionList = revRegionList;
    }

    public List<NodeDirectRelation> getDirectList() {
        return directList;
    }

    public void setDirectList(List<NodeDirectRelation> directList) {
        this.directList = directList;
    }

    public List<Node> getRevDirectList() {
        return revDirectList;
    }

    public void setRevDirectList(List<Node> revDirectList) {
        this.revDirectList = revDirectList;
    }

    public double getDensity() {
        return density;
    }

    public void setDensity(double density) {
        this.density = density;
    }

    public List<ReviewDTO> getAllReviews() {
        List<ReviewDTO> reviews = new ArrayList<>();
        for (NodeDirectRelation direct : directList) {
            reviews.addAll(direct.getReviews());
        }
        return reviews;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 23 * hash + Objects.hashCode(this.providerName);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Node other = (Node) obj;
        return Objects.equals(this.providerName, other.providerName);
    }

    @Override
    public String toString() {
        return "Node{" + "providerName=" + providerName + ", reputation=" + reputation + ", directList=" + directList + ", revDirectList=" + revDirectList + '}';
    }
}
