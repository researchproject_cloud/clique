/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.clique;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import javax.naming.NamingException;
import model.other.DB;

/**
 * This class is data access object for Clique providing insert,loadAll and
 * createDTO functions
 *
 * @author hien_quan
 */
public class CliqueDAO {
/**
 * This function insert clique into database.
 * @param clique
 * @throws SQLException
 * @throws NamingException 
 */
    public void insert(Clique clique) throws SQLException, NamingException {
        Logger.getLogger(CliqueDAO.class.getName()).info(clique.toString());
        if (findByProviderString(clique.getProvider_string()) != null) {
            return;
        }
        try (DB db = new DB();) {
            db.queryUpdate("insert into clique (provider_string, correct, strong) values (?, ?, ?)",
                    clique.getProvider_string(),
                    clique.isCorrect(),
                    clique.isStrong());
        }
    }
    
    public Clique findByProviderString(String provider_string) throws SQLException, NamingException {
        try (DB db = new DB();) {
        
            ResultSet rs = db.query("select * from clique where provider_string = ?", provider_string);
            while (rs.next()) {
                return createDTO(rs);
            }
        }
        return null;
    }
/**
 * This function return list of cliques
 * @return
 * @throws SQLException
 * @throws NamingException 
 */
    public List<Clique> loadAll() throws SQLException, NamingException {
        List<Clique> cliques = new ArrayList<>();

        try (DB db = new DB();) {
            ResultSet result = db.query("select * from clique");
            while (result.next()) {
                cliques.add(createDTO(result));
            }
        }
        return cliques;
    }
/**
 * This function creates DTO for clique.
 * @param result
 * @return
 * @throws SQLException 
 */
    private Clique createDTO(ResultSet result) throws SQLException {
        Clique clique = new Clique();
        clique.setCorrect(result.getBoolean("correct"));
        clique.setStrong(result.getBoolean("strong"));
        clique.setProvider_string(result.getString("provider_string"));
        return clique;
    }
}
