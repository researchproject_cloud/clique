/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.clique;

import java.util.List;
import model.review.ReviewDTO;

/**
 * This class manages all information of a direct connection between two nodes.
 *
 * @author hien_quan
 */
public class NodeDirectRelation {

    Node toNode;
    double weight;
    List<ReviewDTO> reviews;

    public NodeDirectRelation(Node toNode, double weight, List<ReviewDTO> reviews) {
        this.toNode = toNode;
        this.weight = weight;
        this.reviews = reviews;
    }

    public NodeDirectRelation() {
    }

    public Node getToNode() {
        return toNode;
    }

    public void setToNode(Node toNode) {
        this.toNode = toNode;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public List<ReviewDTO> getReviews() {
        return reviews;
    }

    public void setReviews(List<ReviewDTO> reviews) {
        this.reviews = reviews;
    }
}
