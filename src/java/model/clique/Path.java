package model.clique;

/**
 * This class contains the information of a path in graph, with source node, destination node 
 * and influence between these nodes
 *
 * @author hien_quan
 */
public class Path {

    Node fromNode;
    Node toNode;
    double influence;

    public Path(Node fromNode, Node toNode, double influence) {
        this.fromNode = fromNode;
        this.toNode = toNode;
        this.influence = influence;
    }

    public Node getFromNode() {
        return fromNode;
    }

    public void setFromNode(Node fromNode) {
        this.fromNode = fromNode;
    }

    public Node getToNode() {
        return toNode;
    }

    public void setToNode(Node toNode) {
        this.toNode = toNode;
    }

    public double getInfluence() {
        return influence;
    }

    public void setInfluence(double influence) {
        this.influence = influence;
    }

    @Override
    public String toString() {
        return "Path{fromNode=" + fromNode.getProviderName() + ", toNode=" + toNode.getProviderName() + ", influence=" + influence + '}';
    }
}
