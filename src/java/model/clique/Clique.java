/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.clique;

import java.util.ArrayList;
import java.util.List;

/**
 * This Class is Model object for Clique nodes.
 * @author Hien Quan
 */
public class Clique {
    
    String provider_string = "";
    List<Node> nodes = new ArrayList<>();
    boolean correct = false;
    boolean strong;

    public Clique(List<Node> nodes) {
        this.nodes = nodes;
        provider_string = calculateProviderString();
    }

    public Clique() {
        
    }

    public String getProvider_string() {
        return provider_string;
    }

    public void setProvider_string(String provider_string) {
        this.provider_string = provider_string;
    }

    public boolean isCorrect() {
        return correct;
    }

    public void setCorrect(Boolean correct) {
        this.correct = correct;
    }

    public boolean isStrong() {
        return strong;
    }

    public void setStrong(boolean strong) {
        this.strong = strong;
    }

    public List<Node> getNodes() {
        return nodes;
    }

    public void setNodes(List<Node> nodes) {
        this.nodes = nodes;
    }
    
    private String calculateProviderString() {
        String providerNames = "";
        
        for (Node node : nodes)
            providerNames += node.getProviderName() + ", ";
        
        if (!providerNames.isEmpty()) {
            providerNames = providerNames.substring(0, providerNames.length() - 2);
        }
        
        return providerNames;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Clique other = (Clique) obj;
        return this.provider_string.equals(other.getProvider_string());
    }

    @Override
    public String toString() {
        return "Clique{" + "provider_string=" + provider_string + ", correct=" + correct + ", strong=" + strong + '}';
    }
}
