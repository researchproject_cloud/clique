/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.reviewer;

import model.other.DB;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import javax.naming.NamingException;

/**
 *
 * @author Suzan Aziz
 */
public class ReviewerDAO {

    public ArrayList<ReviewerDTO> loadAll() throws NamingException, SQLException {
        ArrayList<ReviewerDTO> reviewers = new ArrayList<>();
        
        try (DB db = new DB();) {

            ResultSet rs = db.query("select * from reviewer");
            while (rs.next()) {
                reviewers.add(createDTO(rs));
            }
        }
        return reviewers;
    }

    private ReviewerDTO createDTO(ResultSet rs) throws SQLException {
        ReviewerDTO reviewer = new ReviewerDTO();

        reviewer.setName(rs.getString("name"));
        reviewer.setEmail(rs.getString("email"));
        reviewer.setCompany_name(rs.getString("company_name"));
        return reviewer;
    }
}
