package model.reviewer;

import javax.validation.constraints.*;

/**
 * This class is for creating get and set function for Reviewers DB records. 
 * @author Suzan
 */
public class ReviewerDTO {
    private String name;
    private String email;
    private String company_name;
    
    @NotNull
    @Size(max = 255)
    public String getName() {
        return name;
        
    }
    
    public void setName(String name) {
        this.name = name;
    }
    
    @NotNull
    @Size(max = 255) 
    public String getEmail() {
        return email;
    }
    
    @NotNull
    @Size(max = 255)
    public String getCompany_name() {
        return company_name;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setCompany_name(String company_name) {
        this.company_name = company_name;
    }

    @Override
    public String toString() {
        return "ReviewerDTO{" + "name=" + name + ", email=" + email + ", company_name=" + company_name + '}';
    }
    
     
}

