/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.provider;

import javax.validation.constraints.*;

/**
 
 * This class is for creating get and set function for Providers DB records. 
 * @author Suzan
 */
public class ProviderDTO {
    private int id;
    private String company_name;
    private String description;
    private String company_email;
    private String weblink;
    int reputation;

    @NotNull
    @Max(100)
    @Min(0)
    public int getReputation() {
        return reputation;
    }

    public void setReputation(int reputation) {
        this.reputation = reputation;
    }

    @NotNull
    public int getId() {
        return id;
    }
    
    @NotNull
    @Size(max = 255)
    public String getCompany_name() {
        return company_name;
    }

    @Size(max = 255)
    public String getDescription() {
        return description;
    }

    @Size(max = 255)
    public String getCompany_email() {
        return company_email;
    }

    @Size(max = 255)
    public String getWeblink() {
        return weblink;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setCompany_name(String company_name) {
        this.company_name = company_name;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setCompany_email(String company_email) {
        this.company_email = company_email;
    }

    public void setWeblink(String weblink) {
        this.weblink = weblink;
    }

    @Override
    public String toString() {
        return "ProviderDTO{" + "id=" + id + ", company_name=" + company_name + ", description=" + description + ", company_email=" + company_email + ", weblink=" + weblink + '}';
    }
    
    
    
    
}



