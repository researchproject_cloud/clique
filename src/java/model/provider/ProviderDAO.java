/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.provider;

import model.other.DB;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import javax.naming.NamingException;

/**
 *
 * @author Suzan Aziz
 */
public class ProviderDAO {

    public ArrayList<ProviderDTO> loadAll() throws NamingException, SQLException {
        ArrayList<ProviderDTO> providers = new ArrayList<>();

        try (DB db = new DB();) {

            ResultSet rs = db.query("select * from provider");
            while (rs.next()) {
                providers.add(createDTO(rs));
            }
        }
        return providers;
    }
    
    public void updateReputation(ProviderDTO provider) throws SQLException, NamingException {
        try (DB db = new DB();) {
            db.queryUpdate("update provider "
                        + "set reputation=? "
                        + "where company_name=?", provider.getReputation(), provider.getCompany_name());
        }
    }

    private ProviderDTO createDTO(ResultSet rs) throws SQLException {
        ProviderDTO provider = new ProviderDTO();

        provider.setId(rs.getInt("id"));
        provider.setCompany_name(rs.getString("company_name"));
        provider.setDescription(rs.getString("description"));
        provider.setCompany_email(rs.getString("company_email"));
        provider.setWeblink(rs.getString("weblink"));
        provider.setReputation(rs.getInt("reputation"));
        return provider;
    }
}
