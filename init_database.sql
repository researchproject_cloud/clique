drop table review;
drop table reviewer;
drop table provider;
drop table clique;

create table review (
    id int not null primary key,
    reviewer varchar(255) not null,
    content varchar(5000) not null,
    review_datetime date not null,
    rating_star int not null,
    for_provider varchar(255) not null        
);

create table reviewer (
    name varchar(255) not null, 
    email varchar(255) not null primary key,
    company_name varchar(255) not null
);

 create table provider (
    id int not null primary key,
    company_name varchar(255) not null, 
    description varchar(2000) not null,
    company_email varchar(255),
    weblink varchar(255),
    reputation int not null
);

create table clique (
    provider_string varchar(2000) primary key,
    correct boolean,
    strong boolean
);