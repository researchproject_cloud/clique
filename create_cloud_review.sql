--Old version of sql file for creating DB

--User Details
 

create table review (
    id int not null primary key,
    reviewer varchar(255) not null,
    content varchar(5000) not null,
    review_datetime date not null,
    rating_star int not null,
    for_provider varchar(255) not null        
);

create table reviewer (
    name varchar(255) not null, 
    email varchar(255) not null primary key,
    company_name varchar(255) not null
);

 create table provider (
    id int not null primary key,
    company_name varchar(255) not null, 
    description varchar(2000) not null,
    company_email varchar(255),
    weblink varchar(255),
    reputation int not null
);

insert into provider(id,company_name,description, company_email,weblink, reputation) values
    (1, 'Dreamhost', 'Open source Cloud Computing service','','www.dreamhost.com', 100);

insert into provider(id,company_name,description, company_email,weblink, reputation) values
    (2, 'Ambit Energy', 'Electricity and Gas provider','','http://ww2.ambitenergy.com/', 100);

insert into provider(id,company_name,description, company_email,weblink, reputation) values
    (3, 'Ebay', 'Electricity and Gas provider','','http://ww2.ambitenergy.com/', 100);

insert into reviewer(name,email,company_name) values
    ('Neal Smith', 'neal.smith@yahoo.com','Ambit Energy');

insert into reviewer(name,email,company_name) values
    ('John Baker', 'john.baker@yahoo.com','Ambit Energy');

insert into reviewer(name,email,company_name) values
    ('Helen Baker', 'helen.baker@yahoo.com','Dreamhost');

   
insert into review(id, reviewer, content, review_datetime,rating_star,for_provider) values
    (1,'neal.smith@yahoo.com', 'I found Dreamhost to be extremely useful. The support team is very helpful and don''t mind answering a few questions for beginners. I would definitely recommend Dreamhost to my family and friends in the future.',{d '2013-03-25'},4,'Dreamhost');

insert into review(id, reviewer, content, review_datetime,rating_star,for_provider) values
    (5, 'john.baker@yahoo.com', 'Dreamhost is really good', {d '2013-04-25'}, 4,'Ebay' );


insert into review(id, reviewer, content, review_datetime,rating_star,for_provider) values
    (3,'helen.baker@yahoo.com', 'I found Dreamhost to be extremely useful. The support team is very helpful and don''t mind answering a few questions for beginners. I would definitely recommend Dreamhost to my family and friends in the future.',{d '2013-03-25'},4,'Ambit Energy');


insert into review(id, reviewer, content, review_datetime,rating_star,for_provider) values
    (6, 'john.baker@yahoo.com', 'Dreamhost is really good', {d '2013-04-25'}, 4,'Ebay' );

insert into review(id, reviewer, content, review_datetime,rating_star,for_provider) values
    (4, 'helen.baker@yahoo.com', 'Dreamhost is really good', {d '2013-04-25'}, 4,'Ambit Energy' );


insert into review(id, reviewer, content, review_datetime,rating_star,for_provider) values
    (2, 'john.baker@yahoo.com', 'Dreamhost is really good', {d '2013-04-25'}, 4,'Dreamhost' );


